#
# TABLE STRUCTURE FOR: base_tb1
#

DROP TABLE IF EXISTS `base_tb1`;

CREATE TABLE `base_tb1` (
  `nomer` int(11) NOT NULL AUTO_INCREMENT,
  `waktu` datetime DEFAULT NULL,
  `ws` float DEFAULT NULL,
  `wd` float DEFAULT NULL,
  `rf` float DEFAULT NULL,
  `sr` float DEFAULT NULL,
  `temp` float DEFAULT NULL,
  `hum` float DEFAULT NULL,
  `press` float DEFAULT NULL,
  `pm25` float DEFAULT NULL,
  `pm10` float DEFAULT NULL,
  `co` float DEFAULT NULL,
  `o3` float DEFAULT NULL,
  `no2` float DEFAULT NULL,
  `so2` float DEFAULT NULL,
  `shc` float DEFAULT NULL,
  PRIMARY KEY (`nomer`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

INSERT INTO `base_tb1` (`nomer`, `waktu`, `ws`, `wd`, `rf`, `sr`, `temp`, `hum`, `press`, `pm25`, `pm10`, `co`, `o3`, `no2`, `so2`, `shc`) VALUES (1, '2021-03-31 14:37:48', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15');
INSERT INTO `base_tb1` (`nomer`, `waktu`, `ws`, `wd`, `rf`, `sr`, `temp`, `hum`, `press`, `pm25`, `pm10`, `co`, `o3`, `no2`, `so2`, `shc`) VALUES (4, '2021-03-31 15:31:37', '0', '20', '0', '157', '24', '89', '924', '9', '9', '-19126', '-161', '-1209', '-3797', '47283');
INSERT INTO `base_tb1` (`nomer`, `waktu`, `ws`, `wd`, `rf`, `sr`, `temp`, `hum`, `press`, `pm25`, `pm10`, `co`, `o3`, `no2`, `so2`, `shc`) VALUES (5, '2021-03-31 15:36:20', '0', '20', '0', '157', '24', '89', '924', '9', '9', '-19126', '-161', '-1209', '-3797', '47283');
INSERT INTO `base_tb1` (`nomer`, `waktu`, `ws`, `wd`, `rf`, `sr`, `temp`, `hum`, `press`, `pm25`, `pm10`, `co`, `o3`, `no2`, `so2`, `shc`) VALUES (6, '0000-00-00 00:00:00', '10', '9', '18', '0', '80', '8', '8', '29', '29', '29', '21', '21', '21', '21');
INSERT INTO `base_tb1` (`nomer`, `waktu`, `ws`, `wd`, `rf`, `sr`, `temp`, `hum`, `press`, `pm25`, `pm10`, `co`, `o3`, `no2`, `so2`, `shc`) VALUES (8, '2021-04-16 00:00:00', '12', '12', '34', '0', '12', '34', '123', '123321', '1231', '3123', '31231', '3131', '1323', '31');


#
# TABLE STRUCTURE FOR: level
#

DROP TABLE IF EXISTS `level`;

CREATE TABLE `level` (
  `id_level` int(11) NOT NULL AUTO_INCREMENT,
  `nama_level` varchar(20) NOT NULL,
  PRIMARY KEY (`id_level`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

INSERT INTO `level` (`id_level`, `nama_level`) VALUES (1, 'admin');
INSERT INTO `level` (`id_level`, `nama_level`) VALUES (2, 'checker');
INSERT INTO `level` (`id_level`, `nama_level`) VALUES (3, 'owner');
INSERT INTO `level` (`id_level`, `nama_level`) VALUES (4, 'pelanggan');


#
# TABLE STRUCTURE FOR: user
#

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(10) NOT NULL,
  `password` varchar(20) NOT NULL,
  `nama_user` varchar(20) NOT NULL,
  `id_level` int(20) NOT NULL,
  `user_create` varchar(50) NOT NULL,
  `create_date` date NOT NULL,
  `user_update` varchar(50) NOT NULL,
  `update_date` date NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

INSERT INTO `user` (`id_user`, `username`, `password`, `nama_user`, `id_level`, `user_create`, `create_date`, `user_update`, `update_date`) VALUES (1, 'dani', '123qwerty', 'dani hamdani', 1, '', '0000-00-00', 'dani hamdani', '2021-04-07');
INSERT INTO `user` (`id_user`, `username`, `password`, `nama_user`, `id_level`, `user_create`, `create_date`, `user_update`, `update_date`) VALUES (14, 'hamdani', '071119', 'hamdan', 2, 'dani hamdani', '2021-04-07', '', '0000-00-00');


