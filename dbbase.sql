-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 08, 2021 at 04:31 AM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 7.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbbase`
--

-- --------------------------------------------------------

--
-- Table structure for table `base_tb1`
--

CREATE TABLE `base_tb1` (
  `nomer` int(11) NOT NULL,
  `waktu` datetime DEFAULT NULL,
  `ws` float DEFAULT NULL,
  `wd` float DEFAULT NULL,
  `rf` float DEFAULT NULL,
  `sr` float DEFAULT NULL,
  `temp` float DEFAULT NULL,
  `hum` float DEFAULT NULL,
  `press` float DEFAULT NULL,
  `pm25` float DEFAULT NULL,
  `pm10` float DEFAULT NULL,
  `co` float DEFAULT NULL,
  `o3` float DEFAULT NULL,
  `no2` float DEFAULT NULL,
  `so2` float DEFAULT NULL,
  `shc` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `base_tb1`
--

INSERT INTO `base_tb1` (`nomer`, `waktu`, `ws`, `wd`, `rf`, `sr`, `temp`, `hum`, `press`, `pm25`, `pm10`, `co`, `o3`, `no2`, `so2`, `shc`) VALUES
(1, '2021-03-31 14:37:48', 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15),
(4, '2021-03-31 15:31:37', 0, 20, 0, 157, 24, 89, 924, 9, 9, -19126, -161, -1209, -3797, 47283),
(5, '2021-03-31 15:36:20', 0, 20, 0, 157, 24, 89, 924, 9, 9, -19126, -161, -1209, -3797, 47283),
(6, '0000-00-00 00:00:00', 10, 9, 18, 0, 80, 8, 8, 29, 29, 29, 21, 21, 21, 21),
(8, '2021-04-16 00:00:00', 12, 12, 34, 0, 12, 34, 123, 123321, 1231, 3123, 31231, 3131, 1323, 31);

-- --------------------------------------------------------

--
-- Table structure for table `level`
--

CREATE TABLE `level` (
  `id_level` int(11) NOT NULL,
  `nama_level` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `level`
--

INSERT INTO `level` (`id_level`, `nama_level`) VALUES
(1, 'admin'),
(2, 'checker'),
(3, 'owner'),
(4, 'pelanggan');

-- --------------------------------------------------------

--
-- Stand-in structure for view `login`
-- (See below for the actual view)
--
CREATE TABLE `login` (
`id_user` int(11)
,`username` varchar(10)
,`password` varchar(20)
,`nama_user` varchar(20)
,`user_create` varchar(50)
,`create_date` date
,`user_update` varchar(50)
,`update_date` date
,`id_level` int(11)
,`nama_level` varchar(20)
);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `username` varchar(10) NOT NULL,
  `password` varchar(20) NOT NULL,
  `nama_user` varchar(20) NOT NULL,
  `id_level` int(20) NOT NULL,
  `user_create` varchar(50) NOT NULL,
  `create_date` date NOT NULL,
  `user_update` varchar(50) NOT NULL,
  `update_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `username`, `password`, `nama_user`, `id_level`, `user_create`, `create_date`, `user_update`, `update_date`) VALUES
(1, 'dani', '123qwerty', 'dani hamdani', 1, '', '0000-00-00', 'dani hamdani', '2021-04-07'),
(14, 'hamdani', '071119', 'hamdan', 2, 'dani hamdani', '2021-04-07', '', '0000-00-00');

-- --------------------------------------------------------

--
-- Structure for view `login`
--
DROP TABLE IF EXISTS `login`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `login`  AS SELECT `user`.`id_user` AS `id_user`, `user`.`username` AS `username`, `user`.`password` AS `password`, `user`.`nama_user` AS `nama_user`, `user`.`user_create` AS `user_create`, `user`.`create_date` AS `create_date`, `user`.`user_update` AS `user_update`, `user`.`update_date` AS `update_date`, `level`.`id_level` AS `id_level`, `level`.`nama_level` AS `nama_level` FROM (`user` left join `level` on(`user`.`id_level` = `level`.`id_level`)) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `base_tb1`
--
ALTER TABLE `base_tb1`
  ADD PRIMARY KEY (`nomer`);

--
-- Indexes for table `level`
--
ALTER TABLE `level`
  ADD PRIMARY KEY (`id_level`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `base_tb1`
--
ALTER TABLE `base_tb1`
  MODIFY `nomer` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `level`
--
ALTER TABLE `level`
  MODIFY `id_level` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
