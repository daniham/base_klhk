<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class UserModel extends CI_Model
{
	public function get($username)
	{
		$this->db->where('username', $username); // Untuk menambahkan Where Clause : username='$username'
		$result = $this->db->get('login')->row(); // Untuk mengeksekusi dan mengambil data hasil query
		return $result;
	}

	#untuk pengguna
	#digunakan untuk mengambil data saat edit dan hapus
	public function getDatapengguna($id_user)
	{
		return $this->db->get_where('user', ['id_user' => $id_user])->row_array();
	}
	public function getDatapengguna2($id)
	{
		if ($id != NULL) {
			$this->db->like('username', $id)->or_like('nama_user', $id);
			return $this->db->get('user')->result_array();
		} else {
			return $this->db->get('user')->result_array();
		}
	}
	#untuk menampilkan seluruh data di tabel
	public function getAllDatapengguna()
	{
		return $this->db->get('login')->result_array();
	}
	public function tambahDatapengguna()
	{
		$data = [
			'username' => htmlspecialchars($this->input->post('username', ENT_QUOTES, 'UTF-8', true)),
			'password' => htmlspecialchars($this->input->post('password', ENT_QUOTES, 'UTF-8', true)),
			'nama_user' => htmlspecialchars($this->input->post('nama_user', ENT_QUOTES, 'UTF-8', true)),
			'id_level' => htmlspecialchars($this->input->post('id_level', ENT_QUOTES, 'UTF-8', true)),
			'user_create' => htmlspecialchars($this->input->post('user_create', ENT_QUOTES, 'UTF-8', true)),
			'create_date' => htmlspecialchars($this->input->post('create_date', ENT_QUOTES, 'UTF-8', true))
		];

		$data = $this->security->xss_clean($data);
		return $this->db->insert('user', $data);
	}
	public function hapusDatapengguna($id_user)
	{
		$this->db->delete('user', ['id_user' => $id_user]);
	}
	public function ubahDatapengguna()
	{
		$data = [
			'username' => htmlspecialchars($this->input->post('username', ENT_QUOTES, 'UTF-8', true)),
			'password' => htmlspecialchars($this->input->post('password', ENT_QUOTES, 'UTF-8', true)),
			'nama_user' => htmlspecialchars($this->input->post('nama_user', ENT_QUOTES, 'UTF-8', true)),
			'id_level' => htmlspecialchars($this->input->post('id_level', ENT_QUOTES, 'UTF-8', true)),
			'user_update' => htmlspecialchars($this->input->post('user_update', ENT_QUOTES, 'UTF-8', true)),
			'update_date' => htmlspecialchars($this->input->post('update_date', ENT_QUOTES, 'UTF-8', true))
		];
		$data = $this->security->xss_clean($data);
		$this->db->where('id_user', $this->input->post('id_user'));
		$this->db->update('user', $data);
	}
	public function gantiPasswordNow()
	{
		$data = [
			'username' => htmlspecialchars($this->input->post('username', ENT_QUOTES, 'UTF-8', true)),
			'password' => htmlspecialchars($this->input->post('password', ENT_QUOTES, 'UTF-8', true)),
			'nama_user' => htmlspecialchars($this->input->post('nama_user', ENT_QUOTES, 'UTF-8', true)),
			'id_level' => htmlspecialchars($this->input->post('id_level', ENT_QUOTES, 'UTF-8', true)),
			'user_update' => htmlspecialchars($this->input->post('user_update', ENT_QUOTES, 'UTF-8', true)),
			'update_date' => htmlspecialchars($this->input->post('update_date', ENT_QUOTES, 'UTF-8', true))
		];
		$data = $this->security->xss_clean($data);
		$this->db->where('id_user', $this->input->post('id_user'));
		$this->db->update('user', $data);
	}

	public function authenticated()
	{ // Fungsi ini berguna untuk mengecek apakah user sudah login atau belum
		// Pertama kita cek dulu apakah controller saat ini yang sedang diakses user adalah controller Auth apa bukan
		// Karena fungsi cek login hanya kita berlakukan untuk controller selain controller Auth
		if ($this->uri->segment(1) != 'auth' && $this->uri->segment(1) != '') {
			// Cek apakah terdapat session dengan nama authenticated
			if (!$this->session->userdata('authenticated')) // Jika tidak ada / artinya belum login
				redirect('auth'); // Redirect ke halaman login
		}
	}
	function get_data2($table)
	{
		return $this->db->get($table);
	}
	public function getAllBaseTb()
	{
		return $this->db->get('base_tb1')->result_array();
	}
	public function hapusBaseTb($nomer)
	{
		$this->db->delete('base_tb1', ['nomer' => $nomer]);
	}

	public function ubahBaseTb()
	{
		$data = [
			'waktu' => htmlspecialchars($this->input->post('waktu', ENT_QUOTES, 'UTF-8', true)),
			'ws' => htmlspecialchars($this->input->post('ws', ENT_QUOTES, 'UTF-8', true)),
			'wd' => htmlspecialchars($this->input->post('wd', ENT_QUOTES, 'UTF-8', true)),
			'rf' => htmlspecialchars($this->input->post('rf', ENT_QUOTES, 'UTF-8', true)),
			'sr' => htmlspecialchars($this->input->post('sr', ENT_QUOTES, 'UTF-8', true)),
			'temp' => htmlspecialchars($this->input->post('temp', ENT_QUOTES, 'UTF-8', true)),
			'hum' => htmlspecialchars($this->input->post('hum', ENT_QUOTES, 'UTF-8', true)),
			'press' => htmlspecialchars($this->input->post('press', ENT_QUOTES, 'UTF-8', true)),
			'pm25' => htmlspecialchars($this->input->post('pm25', ENT_QUOTES, 'UTF-8', true)),
			'pm10' => htmlspecialchars($this->input->post('pm10', ENT_QUOTES, 'UTF-8', true)),
			'co' => htmlspecialchars($this->input->post('co', ENT_QUOTES, 'UTF-8', true)),
			'o3' => htmlspecialchars($this->input->post('o3', ENT_QUOTES, 'UTF-8', true)),
			'no2' => htmlspecialchars($this->input->post('no2', ENT_QUOTES, 'UTF-8', true)),
			'so2' => htmlspecialchars($this->input->post('so2', ENT_QUOTES, 'UTF-8', true)),
			'shc' => htmlspecialchars($this->input->post('shc', ENT_QUOTES, 'UTF-8', true))
		];
		$data = $this->security->xss_clean($data);
		$this->db->where('nomer', $this->input->post('nomer'));
		$this->db->update('base_tb1', $data);
	}

	public function getBaseTb($nomer)
	{
		return $this->db->get_where('base_tb1', ['nomer' => $nomer])->row_array();
	}

	public function tambahBaseTb()
	{
		$data = [
			'waktu' => htmlspecialchars($this->input->post('waktu', ENT_QUOTES, 'UTF-8', true)),
			'ws' => htmlspecialchars($this->input->post('ws', ENT_QUOTES, 'UTF-8', true)),
			'wd' => htmlspecialchars($this->input->post('wd', ENT_QUOTES, 'UTF-8', true)),
			'rf' => htmlspecialchars($this->input->post('rf', ENT_QUOTES, 'UTF-8', true)),
			'sr' => htmlspecialchars($this->input->post('sr', ENT_QUOTES, 'UTF-8', true)),
			'temp' => htmlspecialchars($this->input->post('temp', ENT_QUOTES, 'UTF-8', true)),
			'hum' => htmlspecialchars($this->input->post('hum', ENT_QUOTES, 'UTF-8', true)),
			'press' => htmlspecialchars($this->input->post('press', ENT_QUOTES, 'UTF-8', true)),
			'pm25' => htmlspecialchars($this->input->post('pm25', ENT_QUOTES, 'UTF-8', true)),
			'pm10' => htmlspecialchars($this->input->post('pm10', ENT_QUOTES, 'UTF-8', true)),
			'co' => htmlspecialchars($this->input->post('co', ENT_QUOTES, 'UTF-8', true)),
			'o3' => htmlspecialchars($this->input->post('o3', ENT_QUOTES, 'UTF-8', true)),
			'no2' => htmlspecialchars($this->input->post('no2', ENT_QUOTES, 'UTF-8', true)),
			'so2' => htmlspecialchars($this->input->post('so2', ENT_QUOTES, 'UTF-8', true)),
			'shc' => htmlspecialchars($this->input->post('shc', ENT_QUOTES, 'UTF-8', true))
		];
		$data = $this->security->xss_clean($data);
		return $this->db->insert('base_tb1', $data);
	}
	// get data base by waktu
	public function getAllBaseByDate()
	{
		$this->db->order_by('waktu', 'ASC');
		$query = $this->db->get('base_tb1');
		return $query->result_array();
	}
	// get data params
	public function getAllDataParams()
	{
		return $this->db->get('base_tb1')->result_array();
	}
	public function graph()
	{
		$data = $this->db->query("SELECT * from base_tb1");
		return $data->result();
	}
}
