<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Page_User extends MY_Controller
{

  public function __construct()
  {
    parent::__construct();
    $this->load->model('UserModel');
    $this->load->helper('url', 'form');
    //load libary pagination
    $this->load->library('pagination');
  }
    //function memanggil grafick
  public function grafik()
    {
      $data['base_tb1'] = $this->UserModel->graph();
      $this->load->view('templates/header_user');
      $this->load->view('grafik', $data);
      $this->load->view('templates/footer_user');
    }
  public function welcome()
  {
    $this->load->view('templates/header_user');
    $this->load->view('welcome');
    $this->load->view('templates/footer_user');
  }
  public function gantiPassword()
  {
    $this->load->view('templates/header_user');
    $this->authenticated();
    $this->load->view('user/gantiPassword');
    $this->load->view('templates/footer_user');
  }
  public function gantiPasswordNow()
  {
    $this->UserModel->gantiPasswordNow();
    $this->session->set_flashdata('flash_sukses', 'BERHASIL diubah');
    redirect('page_user/welcome');
  }
  public function base()
  {
    $data['base_tb1'] = $this->UserModel->getAllBaseTb();
    $this->load->view('templates/header_user');
    $this->load->view('viewBase', $data);
    $this->load->view('templates/footer_user');
  }
  public function base_tambah()
  {
    $this->UserModel->tambahBaseTb();
    $this->session->set_flashdata('flash_sukses', 'BERHASIL ditambahkan');
    redirect('page_user/base');
  }
  public function base_hapus($nomer)
  {
    $this->UserModel->hapusBaseTb($nomer);
    $this->session->set_flashdata('flash_sukses', 'BERHASIL dihapus');
    redirect('page_user/base');
  }
  public function base_ubah($nomer)
  {
    $data['base_tb1'] = $this->UserModel->getAllBaseTb($nomer);
    $this->load->view('templates/header_user');
    $this->load->view('viewBase_ubah', $data);
    $this->load->view('templates/footer_user');
  }
  public function base_edit()
  {
    $this->UserModel->ubahBaseTb();
    $this->session->set_flashdata('flash_sukses', 'BERHASIL diubah');
    redirect('page_user/base');
  }
}
?>