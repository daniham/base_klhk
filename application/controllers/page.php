<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Page extends MY_Controller
{
 var $API ="";
  public function __construct()
  {
    parent::__construct();
    $this->load->model('UserModel');
    $this->load->helper('url', 'form');
    //load libary pagination
    $this->load->library('pagination');
  }

  //function memanggil grafick
  // public function grafik()
  // {
  //   $data['base_tb1'] = $this->UserModel->getAllBaseByDate();
  //   $data['base_tb1'] = $this->UserModel->getAllDataParams();
  //   $this->load->view('templates/header');
  //   $this->load->view('grafik', $data);
  //   $this->load->view('templates/footer');
  // }
  public function grafik()
	{
		$data['base_tb1'] = $this->UserModel->graph();
    $this->load->view('templates/header');
		$this->load->view('grafik', $data);
    $this->load->view('templates/footer');
	}
  public function welcome()
  {
    $this->load->view('templates/header');
    $this->load->view('welcome');
    $this->load->view('templates/footer');
  }
  public function gantiPassword()
  {
    $this->load->view('templates/header');
    $this->authenticated();
    $this->load->view('gantiPassword');
    $this->load->view('templates/footer');
  }
  public function pengguna()
  {
    $data['admin'] = $this->UserModel->getAllDatapengguna();
    $this->load->view('templates/header');
    $this->authenticated();
    $this->load->view('pengguna', $data);
    $this->load->view('templates/footer');
  }
  
  public function pengguna_tambah()
  {
    $this->UserModel->tambahDatapengguna();
    $this->session->set_flashdata('flash_sukses', 'BERHASIL ditambahkan');
    redirect('page/pengguna');
  }
  public function pengguna_hapus($id_user)
  {
    $this->UserModel->hapusDatapengguna($id_user);
    $this->session->set_flashdata('flash_sukses', 'BERHASIL dihapus');
    redirect('page/pengguna');
  }
  public function pengguna_ubah($id_user)
  {
    $data['user'] = $this->UserModel->getDatapengguna($id_user);
    $this->load->view('templates/header');
    $this->load->view('pengguna_ubah', $data);
    $this->load->view('templates/footer');
  }
  public function pengguna_edit()
  {
    $this->UserModel->ubahDatapengguna();
    $this->session->set_flashdata('flash_sukses', 'BERHASIL diubah');
    redirect('page/pengguna');
  }
  public function base()
  {
    $data['base_tb1'] = $this->UserModel->getAllBaseTb();
    $this->load->view('templates/header');
    $this->load->view('viewBase', $data);
    $this->load->view('templates/footer');
  }
  public function base_tambah()
  {
    $this->UserModel->tambahBaseTb();
    $this->session->set_flashdata('flash_sukses', 'BERHASIL ditambahkan');
    redirect('page/base');
  }
  public function base_hapus($nomer)
  {
    $this->UserModel->hapusBaseTb($nomer);
    $this->session->set_flashdata('flash_sukses', 'BERHASIL dihapus');
    redirect('page/base');
  }
  public function base_ubah($nomer)
  {
    $data['base_tb1'] = $this->UserModel->getAllBaseTb($nomer);
    $this->load->view('templates/header');
    $this->load->view('viewBase_ubah', $data);
    $this->load->view('templates/footer');
  }
  public function base_edit()
  {
    $this->UserModel->ubahBaseTb();
    $this->session->set_flashdata('flash_sukses', 'BERHASIL diubah');
    redirect('page/base');
  }
}
