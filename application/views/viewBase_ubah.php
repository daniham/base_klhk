<div class="container-fluid">
  <div class="row">
    <div class="col">
      <div class="card shadow-lg">
        <div class="card-header bg-gray-500">Data Tabel Base KHLK</div>
        <div class="card-body">
          <form method="post" accept-charset="utf-8" enctype="multipart/form-data" action="<?= base_url('page/base_edit'); ?>">
            <div class="modal-body">
              <div class="form-group">
                <input type="hidden" id="nomer" name="nomer" class="form-control" value="<?= $base_tb1['nomer']; ?>">
                <label class="control-label">Waktu</label>
                <input type="date" class="form-control" id="username" name="username" placeholder="username" value="<?=$base_tb1['username'];?>" required>
                <label>Ws</label>
                <input type="text" class="form-control" id="ws" name="ws" value="<?= $base_tb1['ws']; ?>" required>
                <input type="hidden" class="form-control" id="user_update" name="user_update" value="<?= $this->session->userdata('nama_user'); ?>" required>
                <input name="update_date" type="hidden" id="update_date" value=" <?php echo date('Y-m-d'); ?> " readonly>
              </div>
            </div>

            <div class="modal-footer">
              <a href="<?= base_url('page/base'); ?>" type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-undo"></i>&nbsp;&nbsp;Batal&nbsp;</a>
              <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i>&nbsp;&nbsp;Update</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>