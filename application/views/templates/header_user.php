<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Base  - KLHK</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="<?php echo base_url(); ?>image/png" href="assets/images/icon/favicon.ico">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/themify-icons.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/metisMenu.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/slicknav.min.css">
    <!-- amchart css -->
    <link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
    <!-- others css -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/typography.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/default-css.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/styles.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/responsive.css">
    <link href="<?php echo base_url(); ?>assets/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <script src="<?php echo base_url(); ?>assets/Chart.js"></script>
    <style>
        div.background {
            width: 500px;
            height: 250px;
            background: url(klematis.jpg) repeat;
            border: 2px solid black;
        }

        div.transbox {
            width: 400px;
            height: 180px;
            margin: 30px 50px;
            background-color: #ffffff;
            border: 1px solid black;
            opacity: 0.6;
            filter: alpha(opacity=60);
            /* For IE8 and earlier */
        }

        div.transbox p {
            margin: 30px 40px;
            font-weight: bold;
            color: #000000;
        }

        .showNot {
            display: none;
        }

        @media print {
            .page-title-area {
                display: none !important;
                opacity: 0 !important;
            }

            .sidebar-menu {
                width: 0px !important;
                display: none !important;
                opacity: 0 !important;
            }
        }

        @media screen and (max-width: 700px) {
            .sidebar-menu {
                left: 0px !important;
                display: none;
            }

            .wtfMargin {
                margin-top: -60px;
            }

            .showNot {
                display: block;
            }

            .metismenu {
                overflow: hidden;
            }
        }
    </style>
    <!-- modernizr css -->
    <script src="<?php echo base_url(); ?>assets/js/vendor/modernizr-2.8.3.min.js"></script>
</head>

<body>
    <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <!-- preloader area start -->
    <div id="preloader">
        <div class="loader"></div>
    </div>
    <!-- preloader area end -->
    <!-- page container area start -->
    <div class="page-container">
        <!-- sidebar menu area start -->
        <div class="sidebar-menu" id="main-sidemenu">

            <button class="btn mt-4 ml-4 showNot" onclick="showMenu()"><i class="fa fa-bars"></i></button>
            <div class="sidebar-header">
                <div class="logo wtfMargin">
                    <a href="welcome"><img src="<?php echo base_url(); ?>assets/images/icon/logo.png" alt="logo" style="height: 118px;"></a>
                </div>
            </div>
            <div class="main-menu">
                <div class="menu-inner">
                    <nav>
                        <ul class="metismenu" id="menu">
                            <li class="active">
                                <a href="javascript:void(0)" aria-expanded="true"><i class="ti-dashboard"></i><span>Menu Monitoring</span></a>
                                <ul class="collapse">
                                    <li class="active"><a href="welcome">Dashboard Utama</a></li>
                                    <li><a href="grafik">Graph Data</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="javascript:void(0)" aria-expanded="true"><i class="ti-layout-sidebar-left"></i><span>Manage User
                                    </span></a>
                                <ul class="collapse">
                                    <li><a href="gantiPassword">Ganti Password</a></li>
                                </ul>
                            </li>
                            
                            <li>
                                <a href="javascript:void(0)" aria-expanded="true"><i class="ti-palette"></i><span>View Base
                                    </span></a>
                                <ul class="collapse">
                                    <li><a href="<?php echo base_url('page_user/base'); ?>">Base View</a></li>
                                </ul>
                            </li>
                            <li><a href="../Auth/logout"><i class="fa fa-exclamation-triangle"></i> <span>Logout</span></a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>

        <!-- header area end -->
        <!-- page title area start -->
        <div class="page-title-area mt-3">
            <div class="row align-items-center">
                <div class="col-sm-6">
                    <div class="breadcrumbs-area clearfix">
                        <h4 class="page-title pull-left">Dashboard</h4>
                        <ul class="breadcrumbs pull-left">
                            <li><a href="welcome">Home</a></li>
                            <li><span>Dashboard</span></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-12">
                    <button class="btn btn-primary mt-4 showNot" onclick="showMenu()"><i class="fa fa-bars"></i></button>
                    <div class="user-profile pull-right" style="margin-top: -40px;">
                        <img class="avatar user-thumb" src="<?php echo base_url(); ?>assets/images/author/avatar.png" alt="avatar">
                        <h4 class="user-name dropdown-toggle" data-toggle="dropdown"><?= $_SESSION['nama_user'] ?> <i class="fa fa-angle-down"></i></h4>
                        <div class="dropdown-menu" style="margin-right: -30px;">
                            <a class="dropdown-item" href="#">Message</a>
                            <a class="dropdown-item" href="gantiPassword">Reset Profile</a>
                            <a class="dropdown-item" href="../Auth/logout">Log Out</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- footer area start-->