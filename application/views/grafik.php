<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
 
    <title>grafick Base Klhk</title>
  </head>
  <body>
 
  <div class="container">
    <canvas id="myChart"></canvas>
  </div>
 
  <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
  <script type="text/javascript">
    var ctx = document.getElementById('myChart').getContext('2d');
    var chart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: [
          <?php
            if (count($base_tb1)>0) {
              foreach ($base_tb1 as $data) {
                echo "'" .$data->waktu ."',";
              }
            }
          ?>
        ],
        datasets: [{
            label: 'WS',
            backgroundColor: '#ADD8E6',
            borderColor: '##93C3D2',
            data: [
              <?php
                if (count($base_tb1)>0) {
                   foreach ($base_tb1 as $data) {
                    echo $data->ws . ", ";
                  }
                }
              ?>
            ]
        },{
          label: 'WD',
            backgroundColor: '#00FFFF',
            borderColor: '##93C3D2',
            data: [
              <?php
                if (count($base_tb1)>0) {
                   foreach ($base_tb1 as $data) {
                    echo $data->wd . ", ";
                  }
                }
              ?>
            ]
        },{
          label: 'RF',
            backgroundColor: '#7FFF00',
            borderColor: '##93C3D2',
            data: [
              <?php
                if (count($base_tb1)>0) {
                   foreach ($base_tb1 as $data) {
                    echo $data->rf . ", ";
                  }
                }
              ?>
            ]
        },{
          label: 'SR',
            backgroundColor: '#006400',
            borderColor: '##93C3D2',
            data: [
              <?php
                if (count($base_tb1)>0) {
                   foreach ($base_tb1 as $data) {
                    echo $data->sr . ", ";
                  }
                }
              ?>
            ]
        },{
          label: 'Temp',
            backgroundColor: '#8B0000',
            borderColor: '##93C3D2',
            data: [
              <?php
                if (count($base_tb1)>0) {
                   foreach ($base_tb1 as $data) {
                    echo $data->temp . ", ";
                  }
                }
              ?>
            ]
        },{
          label: 'Hum',
            backgroundColor: '#9400D3',
            borderColor: '##93C3D2',
            data: [
              <?php
                if (count($base_tb1)>0) {
                   foreach ($base_tb1 as $data) {
                    echo $data->hum . ", ";
                  }
                }
              ?>
            ]
        },{
          label: 'Press',
            backgroundColor: '#BDB76B',
            borderColor: '##93C3D2',
            data: [
              <?php
                if (count($base_tb1)>0) {
                   foreach ($base_tb1 as $data) {
                    echo $data->press . ", ";
                  }
                }
              ?>
            ]
        },{
          label: 'Pm25',
            backgroundColor: '#1E90FF',
            borderColor: '##93C3D2',
            data: [
              <?php
                if (count($base_tb1)>0) {
                   foreach ($base_tb1 as $data) {
                    echo $data->pm25 . ", ";
                  }
                }
              ?>
            ]
        },{
          label: 'Pm10',
            backgroundColor: '#FFD700',
            borderColor: '##93C3D2',
            data: [
              <?php
                if (count($base_tb1)>0) {
                   foreach ($base_tb1 as $data) {
                    echo $data->pm10 . ", ";
                  }
                }
              ?>
            ]
        }]
    },
});
 
  </script>
  </body>
</html>