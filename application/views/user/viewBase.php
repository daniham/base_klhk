<div class="flash-data1" data-flashdata="<?= $this->session->flashdata('flash_sukses'); ?>"></div>
<div class="flash-data2" data-flashdata="<?= $this->session->flashdata('flash_gagal'); ?>"></div>

<div class="row" style="margin: 0.5rem;">
  <div class="col py-2">
    <button class="btn btn-info" type="button" data-toggle="collapse" data-target=".multi-collapse" aria-expanded="false" aria-controls="multiCollapseExample1 multiCollapseExample2"><i class="fa fa-plus"></i>&nbsp;&nbsp;Tambah Data 
    </button>
  </div>
</div>

<div class="row" style="margin: 1rem;">
  <div class="col">
    <div class="collapse multi-collapse" id="multiCollapseExample1">
      <div class="card">
        <div class="card-header bg-gray-500">Base Input</div>
        <div class="card-body">
          <form method="post" accept-charset="utf-8" enctype="multipart/form-data" action="<?= base_url('page_user/base_tambah'); ?>">
            <div class="form-group">
            <label>Waktu</label>
              <input type="date" class="form-control form-control-user" id="waktu" name="waktu" placeholder="Masukan Date" required>
            <label>WS</label>
              <input type="text" class="form-control form-control-user" id="ws" name="ws" placeholder="Masukan WS" required>
            <label>WD</label>
              <input type="text" class="form-control form-control-user" id="wd" name="wd" placeholder="Masukan WD" required>
            <label>RF</label>
              <input type="text" class="form-control form-control-user" id="rf" name="rf" placeholder="Masukan RF" required>
            <label>TEMP</label>
              <input type="text" class="form-control form-control-user" id="temp" name="temp" placeholder="Masukan TEMP" required>
            <label>HUMP</label>
              <input type="text" class="form-control form-control-user" id="hum" name="hum" placeholder="Masukan Hum" required>
            <label>PRESS</label>
              <input type="text" class="form-control form-control-user" id="press" name="press" placeholder="Masukan Press" required>
            <label>PM25</label>
              <input type="text" class="form-control form-control-user" id="pm25" name="pm25" placeholder="Masukan PM25" required>
            <label>PM10</label>
              <input type="text" class="form-control form-control-user" id="pm10" name="pm10" placeholder="Masukan PM10" required>
            <label>Co</label>
              <input type="text" class="form-control form-control-user" id="co" name="co" placeholder="Masukan Co" required>
            <label>o3</label>
              <input type="text" class="form-control form-control-user" id="o3" name="o3" placeholder="Masukan o3" required>
            <label>No2</label>
              <input type="text" class="form-control form-control-user" id="no2" name="no2" placeholder="Masukan No2" required>
            <label>So2</label>
              <input type="text" class="form-control form-control-user" id="so2" name="so2" placeholder="Masukan So2" required>
            <label>Shc</label>
              <input type="text" class="form-control form-control-user" id="shc" name="shc" placeholder="Masukan Shc" required>
            </div>
            <button type="submit" class="btn btn-primary"><i class="fa fa-save">&nbsp;&nbsp;Simpan</i></button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row" style="margin: 1rem;">
  <div class="col">
    <div class="collapse multi-collapse show" id="multiCollapseExample2">
      <div class="card shadow-lg">
        <div class="card-header bg-gray-500">Data View Base </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead class="bg-info text-white">
                <tr class="text-center">
                  <th>No </th>
                  <th>Waktu </th>
                  <th>Ws </th>
                  <th>Wd </th>
                  <th>Rf </th>
                  <th>Sr </th>
                  <th>Temp </th>
                  <th>Hump </th>
                  <th>Press </th>
                  <th>pm25 </th>
                  <th>pm10 </th>
                  <th>Co </th>
                  <th>O3 </th>
                  <th>No2 </th>
                  <th>So2 </th>
                  <th>Shc </th>
                  <th>Nomer </th>
                  <th>Aksi </th>
                </tr>
              </thead>
              <tbody>
                <?php
                $no = 0;
                foreach ($base_tb1 as $s) : $no++; ?>
                  <tr>
                    <td class="text-center text-middle"><?= $no; ?></td>
                    <td class="text-middle"><?= $s['waktu']; ?></td>
                    <td class="text-middle"><?= $s['ws']; ?></td>
                    <td class="text-middle"><?= $s['wd']; ?></td>
                    <td class="text-middle"><?= $s['rf']; ?></td>
                    <td class="text-middle"><?= $s['sr']; ?></td>
                    <td class="text-middle"><?= $s['temp']; ?></td>
                    <td class="text-middle"><?= $s['hum']; ?></td>
                    <td class="text-middle"><?= $s['press']; ?></td>
                    <td class="text-middle"><?= $s['pm25']; ?></td>
                    <td class="text-middle"><?= $s['pm10']; ?></td>
                    <td class="text-middle"><?= $s['co']; ?></td>
                    <td class="text-middle"><?= $s['o3']; ?></td>
                    <td class="text-middle"><?= $s['no2']; ?></td>
                    <td class="text-middle"><?= $s['so2']; ?></td>
                    <td class="text-middle"><?= $s['shc']; ?></td>
                    <td class="text-middle"><?= $s['nomer']; ?></td>
                    <td class="text-center text-middle"><a href="<?= base_url('page_user/base_ubah') ?>/<?= $s['nomer']; ?>" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-edit text-success" aria-hidden="true"></i></a>
                      | <a href="<?= base_url('page_user/base_hapus') ?>/<?= $s['nomer']; ?>" class="tombol_hapus" data-toggle="tooltip" data-placement="top" title="Hapus"><i class="fa fa-trash text-danger" aria-hidden="true"></i></a>
                    </td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>